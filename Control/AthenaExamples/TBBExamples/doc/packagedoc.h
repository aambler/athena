/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TBBExamples_page TBBExamples
This package contains HelloWorld-type examples of TBB task scheduling within
an athena Algorithm. 

@section TBBExamples_Examples Examples
 - HelloParForAlg
 - HelloPipeAlg
 - HelloGraphAlg




@section TBBExamples_Refs More Documentation

The code can be browsed using LXR 
(http://alxr.usatlas.bnl.gov/lxr/source/atlas/Control/AthenaExamples/TBBExamples)

More information on TBB is  available from
http://threadingbuildingblocks.org/documentation.php

@author Paolo Calafiura <pcalafiura@lbl.gov>

*/
