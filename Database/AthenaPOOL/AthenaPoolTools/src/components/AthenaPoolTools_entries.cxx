#include "../EventCount.h"
#include "../MetadataTest.h"
#include "../BookkeeperDumper.h"
#include "../RequireUniqueEvent.h"

DECLARE_COMPONENT( EventCount )
DECLARE_COMPONENT( MetadataTest )
DECLARE_COMPONENT( BookkeeperDumper )
DECLARE_COMPONENT( RequireUniqueEvent )

