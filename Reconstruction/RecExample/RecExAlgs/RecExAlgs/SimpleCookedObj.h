/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SIMPLECOOKEDOBJ_H
#define SIMPLECOOKEDOBJ_H 1
#include "AthenaKernel/CLASS_DEF.h"
#include "SGTools/BuiltinsClids.h"

typedef float SimpleCookedObj;

// clid for float is now defined by SGTools
//CLASS_DEF(SimpleCookedObj,245111483,1)

#endif // not SIMPLECOOKEDOBJ_H
